package main

import (
	"flag"
	"io/ioutil"
	"os"

	"code.ivysaur.me/riscvemu"
)

func main() {
	loadAddress := flag.Int64("LoadAddress", 0, "Load address and initial program counter")

	flag.Parse()

	files := flag.Args()
	if len(files) != 1 {
		flag.Usage()
		os.Exit(1)
	}

	sw := riscvemu.NewVirtualEEI()

	// Copy file into working memory
	fb, err := ioutil.ReadFile(files[0])
	if err != nil {
		panic(err)
	}
	_, err = sw.WriteAt(fb, *loadAddress)
	if err != nil {
		panic(err)
	}

	c := riscvemu.NewCPU(&sw)
	c.Pc = uint32(*loadAddress)

	for {
		err := c.Step()
		if err != nil {
			panic(err)
		}
	}
}
