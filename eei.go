package riscvemu

// EEI (Execution Environment Interface) is the enivronment in which RISC-V
// code is run.
type EEI interface {
	ReadByte(addr uint32) (byte, error)
	Read16(addr uint32) (uint16, error)
	Read32(addr uint32) (uint32, error)

	WriteByte(addr uint32, value byte) error
	Write16(addr uint32, value uint16) error
	Write32(addr, value uint32) error

	Syscall() error

	MemFence(opcode uint32) error
}
