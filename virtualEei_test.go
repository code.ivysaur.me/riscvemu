package riscvemu

import (
	"testing"
)

func TestEndianness(t *testing.T) {
	e := NewVirtualEEI()

	err := e.Write32(0, 0x12345678)
	if err != nil {
		t.Fatal(err)
	}

	rw, err := e.Read32(0)
	if err != nil {
		t.Fatal(err)
	}
	if rw != 0x12345678 {
		t.Errorf("got %x expected 0x12345678", rw)
	}

	// In little-endian, the 32-bit value 0x12345678 is written in order with its
	// least-significant bytes first (i.e. in a hex editor: 78563412)

	rb, err := e.ReadByte(0)
	if err != nil {
		t.Fatal(err)
	}
	if rb != 0x78 {
		t.Errorf("got %x expected 0x78", rb)
	}

	rb, err = e.ReadByte(1)
	if err != nil {
		t.Fatal(err)
	}
	if rb != 0x56 {
		t.Errorf("got %x expected 0x56", rb)
	}

	rb, err = e.ReadByte(2)
	if err != nil {
		t.Fatal(err)
	}
	if rb != 0x34 {
		t.Errorf("got %x expected 0x34", rb)
	}

	rb, err = e.ReadByte(3)
	if err != nil {
		t.Fatal(err)
	}
	if rb != 0x12 {
		t.Errorf("got %x expected 0x12", rb)
	}
}
