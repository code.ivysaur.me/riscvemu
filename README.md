# riscvemu

An interpreter for the RV32I instruction set.

Features:
- Supports RV32I base instructions only (so far)
- Page-based virtual memory, with custom memory traps and syscalls
- Install with Go Modules (`go get code.ivysaur.me/riscvemu`)

## License

ISC License

The source code includes quotations from the RISC-V Instruction Set Manual under
the [Creative Commons Attribution 4.0 International License](https://github.com/riscv/riscv-isa-manual/blob/riscv-user-2.2/LICENSE).

## References

- RISC-V Instruction Set Manual: https://github.com/riscv/riscv-isa-manual
